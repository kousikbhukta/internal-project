import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object ReadData {

  def main(args: Array[String]) {
    print("This is first code")

    val conf = new SparkConf()
      .set("spark.app.name", "File from directory")
      .set("spark.master", "local[*]")

    val spark = SparkSession.builder()
      .config(conf)
      .getOrCreate()

//    val df = spark.read.csv("C:\\Sapient\\Personal\\Test2121.csv")
//    df.printSchema()
//    df.show()

    println("Spark session has been started")


  }

}
